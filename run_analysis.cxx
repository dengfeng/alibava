#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include "TH1.h"
#include "TH2.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TSystem.h"
#include "TPaveStats.h"
#include "TCanvas.h"
#include "TApplication.h"
#include "TFile.h"
#include <getopt.h>
#include "inc/DataFileRoot.h"
#include "inc/analysis.h"
#include "inc/Tracer.h"
#include "inc/AtlasStyle.h"
#include "inc/sin_preguntas.h"
#include "inc/utils.h"


int main(int argc, char **argv)
{
    SetAtlasStyle() ;
    std::string channel_list;
    std::string pedestal_file;
    std::string calibration_file;
    std::string input_file;
    std::string output_file("alibava.root");
    std::string prog_name;
    const char *ped_file=0;
    const char *cal_file=0;
    int polarity = -1;
    bool dofit = true;
    double tfirst=20.0, tlast=30.0;
    double seed_cut=5.0, neigh_cut=3.0;

    char c;
    int this_option_optind;
    int option_index;
    int iverbose=0;
    static struct option long_options[] =
    {
     { "out", 1, 0, 'o' }, // Output file
     { "pedestals", 1, 0, 'p' },
     { "calibration", 1, 0,'g' },
     { "map", 1,0,'m'},
     { "debug", 0, &iverbose, 1 },
     { "tdc-low", 1, 0, 'u'},
     { "tdc-high", 1, 0, 'v'},
     { "polarity", 1, 0, 's'},
     { "seed_cut", 1, 0, 'a'},
     { "neigh_cut", 1, 0, 'b'},
     { "channel-list", 1, 0, 'x'},
     { "help", 0, 0, 'h' },
     {0, 0, 0, 0 }
    };


    /*
     *  Get the program name
     */
    prog_name = argv[0];
    std::string::size_type jpos = prog_name.rfind('/');
    if (jpos!=prog_name.npos)
        prog_name = prog_name.substr(jpos+1);

    /*
     * Loop on command line arguments
     */
    while(1)
    {
        this_option_optind = optind ? optind : 1;
        option_index = 0;
        c = getopt_long(argc, argv, ":u:v:n:p:g:m:ho:e:z:", long_options, &option_index);
        if (c==-1)
            break;
        switch (c)
        {
            case 0:
                break;
            case 'o':
                output_file=optarg;
                break;
            case 'p':
                pedestal_file=optarg;
                std::cout << "Pedestal file:" << pedestal_file << std::endl;
                break;
            case 'g':
                calibration_file=optarg;
                std::cout << "Calibration file:" << calibration_file << std::endl;
                break;
            case 'u':
                tfirst = atof(optarg);
                break;
            case 'v':
                tlast = atof(optarg);
                break;
            case 'a':
                seed_cut = atof(optarg);
                break;
            case 'b':
                neigh_cut = atof(optarg);
                break;
            case 's':
                polarity = atoi(optarg);
                break;
            case 'x':
                std::cout << "*** " << optarg << std::endl;
                channel_list = optarg;
                break;
            case '?':
                std::cout << "Something went wrong: optind " << option_index << " - " << argv[option_index]  << std::endl;
                break;
            case 'h':
                std::cout << "Usage: " << prog_name
                          << " [options] " << std::endl
                          << "Posible options: "
                          << std::endl;
                for (int i=0; long_options[i].name; i++)
                    std::cout << "\t--" << long_options[i].name
                              << " , -"
                              << (char) long_options[i].val
                              << std::endl;

                return -1;
                break;
            default:
                std::cout << prog_name << " -> " << (char)optopt << " " << argv[optind]
                                                                               << std::endl;
                return -1;
                break;
        }
    }

    if (optind < argc)
        input_file = argv[optind++];

    if (input_file.empty())
    {
        std::cout << "I need an input file" << std::endl;
        return -1;
    }

    //gROOT->SetBatch(false);
    //TApplication *app = new TApplication("theApp",&argc, argv);
    //gROOT->SetStyle("Plain");
    //gStyle->SetPalette(1);
    init_landau();

    std::string outPath =  "./output/"+output_file.substr(0, output_file.find(".root"))+"/" ;
    if(gSystem->AccessPathName(outPath.c_str()))
      gSystem->Exec(("mkdir -p "+outPath).c_str()) ;
    TFile *ofile = new TFile((outPath+output_file).c_str(), "RECREATE");

    DataFileRoot *A = DataFileRoot::OpenFile(input_file.c_str());

    A->set_cuts(seed_cut, neigh_cut);

    if (!pedestal_file.empty())
    {
        std::cout << "Using pedestal file: " << pedestal_file << std::endl;
        ped_file = pedestal_file.c_str();
    }
    if (!calibration_file.empty())
    {
        std::cout << "Using calibration file " << calibration_file << std::endl;
        cal_file = calibration_file.c_str();
    }
    if (!channel_list.empty())
    {
        std::cout << "Channel List: " << channel_list << std::endl;
        A->add_channel_list( ChanList(channel_list.c_str()) );
    }
    init_landau();
    sin_preguntas(A, input_file.c_str(), cal_file, ped_file, polarity, dofit,tfirst, tlast);
    std::cout << "Saving in " << ofile->GetName() << std::endl;
    ofile->Write();

    std::vector<std::string> textList ;
    TH1D *hPed = (TH1D *)gROOT->FindObject("hPed");
    if(hPed)
      DrawSingleHistOnCanvas(outPath+"hPed", hPed, "hist", textList) ;
    TH1D *hNoise = (TH1D *)gROOT->FindObject("hNoise");
    if(hNoise)
      DrawSingleHistOnCanvas(outPath+"hNoise", hNoise, "hist", textList) ;
    TH1D *hGain = (TH1D *)gROOT->FindObject("hGain");
    if(hGain)
      DrawSingleHistOnCanvas(outPath+"hGain", hGain, "hist", textList) ;
    else
      std::cout<<"No Gain"<<std::endl ;
    TH1D *hSpec = (TH1D *)gROOT->FindObject("hSpec");
    if(hSpec)
      DrawSingleHistOnCanvas(outPath+"hSpec", hSpec, "hist", textList) ;
    TH1D *hSpec_timecut = (TH1D *)gROOT->FindObject("hSpec_timecut");
    //textList.push_back("Spectrum with Time cut ["+std::to_string((int)tfirst)+" , "+std::to_string((int)tlast)+"]") ;
    if(hSpec_timecut)
    {
      TCanvas *c = new TCanvas("c", "", 800, 600) ;
      hSpec_timecut->Draw() ;
      gPad->Update();
      TPaveStats *st = (TPaveStats*)hSpec_timecut->FindObject("stats");
      if(st)
      {
        st->SetShadowColor(0) ;
        st->SetBorderSize(0) ;
        st->SetX1NDC(0.6);
        st->SetX2NDC(0.9);
        st->SetY1NDC(0.7);
        st->SetY2NDC(0.9);
      }
      c->Print((outPath+"hSpec_timecut.pdf").c_str()) ;
      delete c ;
      //DrawSingleHistOnCanvas(outPath+"hSpec_timecut", hSpec_timecut, "hist", textList) ;
    }
    TH1D *hSpec_nclust = (TH1D *)gROOT->FindObject("hSpec_nclust");
    if(hSpec_nclust)
      DrawSingleHistOnCanvas(outPath+"hSpec_nclust", hSpec_nclust, "hist", textList) ;
    TH1D *hSpec_cwidth = (TH1D *)gROOT->FindObject("hSpec_cwidth");
    if(hSpec_cwidth)
      DrawSingleHistOnCanvas(outPath+"hSpec_cwidth", hSpec_cwidth, "hist", textList) ;
    TH1D *_hx_ = (TH1D *)gROOT->FindObject("_hx_");
    if(_hx_)
      DrawSingleHistOnCanvas(outPath+"_hx_", _hx_, "e", textList) ;
    TH1D *_hy_ = (TH1D *)gROOT->FindObject("_hy_");
    if(_hy_)
      DrawSingleHistOnCanvas(outPath+"_hy_", _hy_, "e", textList) ;
    //textList.clear() ;
    TH1D *hcmmd_chip1 = (TH1D *)gROOT->FindObject("hcmmd_chip1");
    if(hcmmd_chip1)
      DrawSingleHistOnCanvas(outPath+"hcmmd_chip1", hcmmd_chip1, "e", textList) ;
    TH1D *hcmmd_chip2 = (TH1D *)gROOT->FindObject("hcmmd_chip2");
    if(hcmmd_chip2)
      DrawSingleHistOnCanvas(outPath+"hcmmd_chip2", hcmmd_chip2, "e", textList) ;
    TProfile *hSpec_time_prof = (TProfile *)gROOT->FindObject("hSpec_time_prof");
    if(hSpec_time_prof)
      DrawSingleProfileOnCanvas(outPath+"hSpec_time_prof", hSpec_time_prof, "e", textList) ;
    TH2D *hSpec_cmmd = (TH2D *)gROOT->FindObject("hSpec_cmmd");
    hSpec_cmmd->GetXaxis()->SetLabelOffset(0.07);
    if(hSpec_cmmd)
      DrawSingleHistOnCanvas(outPath+"hSpec_cmmd", hSpec_cmmd, "lego2z", textList) ;
    TH2D *hSpec_time = (TH2D *)gROOT->FindObject("hSpec_time");
    if(hSpec_time)
      DrawSingleHistOnCanvas(outPath+"hSpec_time", hSpec_time, "lego2z", textList) ;
    TH2D *hSpec_chan = (TH2D *)gROOT->FindObject("hSpec_chan");
    if(hSpec_chan)
      DrawSingleHistOnCanvas(outPath+"hSpec_chan", hSpec_chan, "lego2z", textList) ;
    TH2D *hSpec_all = (TH2D *)gROOT->FindObject("hSpec_all");
    if(hSpec_all)
      DrawSingleHistOnCanvas(outPath+"hSpec_all", hSpec_all, "lego2z", textList) ;

    //app->Run(false);

    ofile->Close();
    delete ofile;
    delete A;
    //delete app;
}
