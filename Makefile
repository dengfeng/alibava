# Directories
OBJ=obj
SRC=src
INC=./inc/

# Setting ROOT variables
ROOTCFLAGS := $(shell root-config --cflags)
ROOTLIBS := $(shell root-config --libs)

LIBS += -lTMVA -lSpectrum

# Setting compiler flags
CXX = g++ -Wall -Wformat=0 -c -g -shared -fPIC
CXXFLAGS = -I$(ROOTCFLAGS) -I$(INC)

LD = g++
LDFLAGS	= $(ROOTLIBS) $(LIBS) -L$(INC) -L$(ROOTCFLAGS)

# List of sources and objects
#sources=$(shell find $(SRC) -type f -name \*.cxx) main.cxx
sources=$(shell find $(SRC) -type f -name \*.cxx) run_analysis.cxx
#includes=$(shell find $(INC) -type f -name \*.h)
objects=$(sources:$(SRC)/%.cxx=$(OBJ)/%.o)

# Default target
default: run_analysis

# Generic rule for objects
$(OBJ)/%.o: $(SRC)/%.cxx $(INC)/%.h 
	${CXX} ${CXXFLAGS} -c $< -o $@

# Rule for main object
$run_analysis: run_analysis.cxx 
	${CXX} ${CXXFLAGS} -c $< -o $@

# Rule for main program
run_analysis: $(objects)
	@echo "objects: $(objects)"
	$(LD) $(LDFLAGS) $^ -o $@

clean:
	rm -rf $(OBJ)/*.o main


