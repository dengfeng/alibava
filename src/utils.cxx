#include "TROOT.h"
#include "TStyle.h"
#include "TPaveStats.h"
#include "utils.h"

TCanvas *create_canvas(const char *name, const char *title, int wx, int wy)
{
    TCanvas *cnvs = (TCanvas *)gROOT->FindObject("name");
    if (cnvs)
        delete cnvs;

    if (wx<0 ||wy<0)
        cnvs = new TCanvas(name, title);
    else
        cnvs = new TCanvas(name, title, wx, wy);

    return cnvs;
}

TH1 *create_h1(const char *name, const char *tit, int n, double x1, double x2)
{
    TH1 *hst = (TH1 *)gROOT->FindObject(name);
    if (hst)
        delete hst;
    
    hst = new TH1D(name, tit, n, x1, x2);
    return hst;
}

TH2 *create_h2(const char *name, const char *tit, int nx, double x1, double x2, int ny, double y1, double y2)
{
    TH2 *hst = (TH2 *)gROOT->FindObject(name);
    if (hst)
        delete hst;
    
    hst = new TH2D(name, tit, nx, x1, x2, ny, y1, y2);
    return hst;
}

TProfile *create_profile(const char *name, const char *tit, int n, double x1, double x2, double y1, double y2)
{
    TProfile *hst = (TProfile *)gROOT->FindObject(name);
    if (hst)
        delete hst;
    
    hst = new TProfile(name, tit, n, x1, x2, y1, y2);
    return hst;
}

TProfile2D *create_profile2d(const char *name, const char *tit, int nx, double x1, double x2, int ny, double y1, double y2)
{
    TProfile2D *hst = (TProfile2D *)gROOT->FindObject(name);
    if (hst)
        delete hst;

    hst = new TProfile2D(name, tit, nx, x1, x2, ny, y1, y2);
    return hst;
}


// Draw Single Histogram
int DrawSingleHistOnCanvas(std::string canvasname, TH1* hist, std::string drawoption, std::vector<std::string> textList, bool logy, bool logx, bool isrectangle)
{
  float height=600,width=600 ;
  if(isrectangle)
    width=800 ;

  hist->SetMarkerStyle(20) ;
  hist->SetMarkerColor(kBlack) ;
  hist->SetLineColor(kBlack) ;

  TCanvas *c = new TCanvas(canvasname.c_str(), "", width, height) ;
  TPad *interpad = new TPad("interpad","interpad",0,0,1,1) ;// For main histo
  interpad->SetLogy(logy) ;
  interpad->SetLogx(logx) ;
  interpad->Draw() ;

  interpad->cd() ;
  hist->Draw(drawoption.c_str()) ;
  gPad->Update();
  TPaveStats *st = (TPaveStats*)hist->FindObject("stats");
  if(st)
  {
    st->SetShadowColor(0) ;
    st->SetBorderSize(0) ;
    st->SetX1NDC(0.6);
    st->SetX2NDC(0.9);
    st->SetY1NDC(0.7);
    st->SetY2NDC(0.9);
  }

  //ATLASLabel(0.2, 0.86, "Internal");
  for(int i=0 ; i<(int)textList.size() ; i++)
    myText(0.4,  0.9-0.06*(i+1), 1, textList.at(i).c_str());
  c->Print((canvasname+".pdf").c_str()) ;
  delete c ;
  return 0 ;
}

int DrawSingleHistOnCanvas(std::string canvasname, TH2* hist, std::string drawoption, std::vector<std::string> textList, bool logz, bool logy, bool logx, bool isrectangle)
{
  float height=600,width=600 ;
  if(isrectangle)
    width=800 ;

  hist->SetMarkerStyle(20) ;
  hist->SetMarkerColor(kBlack) ;
  hist->SetLineColor(kBlack) ;
  hist->GetXaxis()->SetLabelSize(0.05) ;
  hist->GetXaxis()->SetTitleSize(0.04) ;
  hist->GetXaxis()->SetTitleOffset(1.5);
  hist->GetXaxis()->CenterTitle() ;
  hist->GetYaxis()->SetTitleSize(0.04) ;
  hist->GetYaxis()->SetLabelSize(0.04) ;
  hist->GetYaxis()->SetTitleOffset(1.7);
  hist->GetYaxis()->CenterTitle() ;
  hist->GetZaxis()->SetTitleSize(0.04) ;
  hist->GetZaxis()->SetLabelSize(0.04) ;
  hist->GetZaxis()->SetTitleOffset(1.5);
  hist->GetZaxis()->SetNdivisions(505) ;

  TCanvas *c = new TCanvas(canvasname.c_str(), "", width, height) ;
  TPad *interpad = new TPad("interpad","interpad",0,0,1,1) ;
  interpad->SetTopMargin(0.05);
  interpad->SetRightMargin(0.17);
  interpad->SetBottomMargin(0.1);
  interpad->SetLeftMargin(0.13);
  interpad->SetLogy(logz) ;
  interpad->SetLogy(logy) ;
  interpad->SetLogx(logx) ;
  interpad->Draw() ;

  interpad->cd() ;
  hist->Draw(drawoption.c_str()) ;

  //ATLASLabel(0.2, 0.86, "Internal");
  for(int i=0 ; i<(int)textList.size() ; i++)
    myText(0.2,  0.86-0.06*(i+1), 1, textList.at(i).c_str());
  c->Print((canvasname+".pdf").c_str()) ;
  delete c ;
  return 0 ;
}
int DrawSingleProfileOnCanvas(std::string canvasname, TProfile* profile, std::string drawoption, std::vector<std::string> textList, bool logy, bool logx, bool isrectangle)
{
  float height=600,width=600 ;
  if(isrectangle)
    width=800 ;

  profile->SetMarkerStyle(20) ;
  profile->SetMarkerColor(kBlack) ;
  profile->SetLineColor(kBlack) ;

  TCanvas *c = new TCanvas(canvasname.c_str(), "", width, height) ;
  TPad *interpad = new TPad("interpad","interpad",0,0,1,1) ;
  interpad->SetLogy(logy) ;
  interpad->SetLogx(logx) ;
  interpad->Draw() ;

  interpad->cd() ;
  gStyle->SetOptStat(0);
  gStyle->SetOptFit(1);
  profile->Draw(drawoption.c_str()) ;

  for(int i=0 ; i<(int)textList.size() ; i++)
    myText(0.2,  0.86-0.06*(i+1), 1, textList.at(i).c_str());
  c->Print((canvasname+".pdf").c_str()) ;
  delete c ;
  return 0 ;
}
